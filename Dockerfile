FROM maven:3-jdk-14 AS mavenBuild

COPY . .
RUN mvn clean install -U

FROM tomcat:9-jdk14-openjdk-oracle

WORKDIR /usr/local/tomcat/webapps

COPY --from=mavenBuild ./movie-ws/target/*.war ./movie-ws.war
