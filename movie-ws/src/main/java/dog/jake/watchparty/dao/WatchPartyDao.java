package dog.jake.watchparty.dao;

import com.mongodb.client.FindIterable;
import dog.jake.watchparty.watchpartyutils.MongoWatchParty;
import org.bson.Document;
import org.bson.types.ObjectId;

public interface WatchPartyDao {
    void insertWatchParty(MongoWatchParty mongoWatchParty);

    Document getWatchPartyById(ObjectId mongoId);

    FindIterable<Document> getWatchPartiesForUser(ObjectId username);
}
