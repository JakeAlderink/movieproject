package dog.jake.watchparty.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dog.jake.watchparty.watchpartyutils.MongoWatchParty;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class MongoWatchPartyDao implements WatchPartyDao{

    private final MongoDatabase database;
    private final MongoClient mongoClient;
    private final MongoCollection<Document> collection;
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    public MongoWatchPartyDao() {
        mongoClient = MongoClients.create("mongodb+srv://movie-ws:movie-ws-password@moviecluster-xbgjb.mongodb.net/test?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("dog_database");
        collection = database.getCollection("watch_parties");
    }


    @Override
    public void insertWatchParty(MongoWatchParty mongoWatchParty) {
        Map<String, Object> map = mapper.convertValue(mongoWatchParty, Map.class);
        Document document  = new Document(map);
        collection.insertOne(document);
    }

    @Override
    public Document getWatchPartyById(ObjectId mongoId) {
        return collection.find(eq("_id", mongoId.toString())).first();
    }

    @Override
    public FindIterable<Document> getWatchPartiesForUser(ObjectId username) {
        return collection.find(eq("users", username.toString()));
    }

}
