package dog.jake.watchparty;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import dog.jake.watchparty.dao.MongoWatchPartyDao;
import dog.jake.watchparty.dao.WatchPartyDao;

import javax.inject.Named;

@Module
public abstract class WatchPartyModule {
    @Binds
    abstract WatchPartyDao bindMongoWatchPartyDao(MongoWatchPartyDao mongoWatchPartyDao);

    @Binds
    @IntoSet
    @Named("instances")
    abstract Object resource(WatchPartyResource watchPartyResource);
}
