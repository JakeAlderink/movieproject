package dog.jake.watchparty;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dog.jake.watchparty.watchpartyutils.ExternalFacingMovieParty;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("/watchparty")
@Produces(MediaType.APPLICATION_JSON)
public class WatchPartyResource {
    private final WatchPartyService watchPartyService;
    private final ObjectMapper mapper;

    @Inject
    public WatchPartyResource(WatchPartyService watchPartyService) {
        this.watchPartyService = watchPartyService;
        this.mapper = new ObjectMapper();
    }

    @Path("/getWatchPartiesByUser/{name}")
    @GET
    @Operation(summary = "get All watch parties a user has been a part of")
    @Tag(name= "Watch Party")
    public Response getWatchPartiesByName(@PathParam("name") String username) {
        List<ExternalFacingMovieParty> externalWatchParties = watchPartyService.getWatchParties(username);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(externalWatchParties)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }


    @Path("/getWatchPartyById/{id}")
    @GET
    @Operation(summary = "get a watch party object by the internal mongoID")
    @Tag(name = "Watch Party")
    public Response getWatchPartyById(@PathParam("id") String mongoId) {
        ObjectId objectId = new ObjectId(mongoId);
        ExternalFacingMovieParty externalWatchParty = watchPartyService.getWatchParty(objectId);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(externalWatchParty)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }


    @Path("/insertWatchParty")
    @POST
    @Operation(summary = "insert a new watch party, ensure that usernames and ratings are entered in the right order")
    @Tag(name = "Watch Party")
    public Response insertNewWatchParty(@QueryParam("usernames") List<String> usernames,
                                        @QueryParam("ratings") List<Double> ratings,
                                        @QueryParam("movieName") String movie,
                                        @Parameter(description = "needs to be in format \"YYYY-MM-DD\"") @QueryParam("date") String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateObj = null;
        try {
            dateObj = dateFormat.parse(date);
        } catch (ParseException e) {
            Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        watchPartyService.insertWatchParty(usernames, ratings, movie, dateObj);
        return Response.status(Response.Status.CREATED).build();

    }

}
