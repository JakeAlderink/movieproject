package dog.jake.watchparty;

import com.mongodb.client.FindIterable;
import dog.jake.movie.MovieService;
import dog.jake.ratings.RatingService;
import dog.jake.users.UserService;
import dog.jake.users.userutils.User;
import dog.jake.watchparty.dao.WatchPartyDao;
import dog.jake.watchparty.watchpartyutils.CustomWatchPartyDeserializer;
import dog.jake.watchparty.watchpartyutils.ExternalFacingMovieParty;
import dog.jake.watchparty.watchpartyutils.InternalWatchParty;
import dog.jake.watchparty.watchpartyutils.MongoWatchParty;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class WatchPartyService {
    private final Logger LOG = LoggerFactory.getLogger(WatchPartyService.class);
    private final WatchPartyDao watchPartyDao;
    private final RatingService ratingService;
    private final MovieService movieService;
    private final UserService userService;
    private final InternalWatchParty.InternalWatchPartyBuilder watchPartyBuilder;

    @Inject
    public WatchPartyService(WatchPartyDao watchPartyDao, RatingService ratingService, MovieService movieService, UserService userService) {
        this.watchPartyDao = watchPartyDao;
        this.ratingService = ratingService;
        this.movieService = movieService;
        this.watchPartyBuilder = new InternalWatchParty.InternalWatchPartyBuilder(ratingService);
        this.userService = userService;
    }


    public void insertWatchParty(List<String> usernames, List<Double> ratings, String movie, Date dateObj) {
        LOG.debug("Inserting new Watch party for movie: {}", movie);
        List<ObjectId> ratingsMongoIds = new ArrayList<>();
        List<ObjectId> userIds = new ArrayList<>();
        for (int i = 0; i < ratings.size(); i++) {
            ratingsMongoIds.add(ratingService.insertRating(ratings.get(i), movie, usernames.get(i), dateObj));
            userIds.add(userService.getUserByName(usernames.get(i)).get_id());
        }
        ObjectId movieId = movieService.getMovie(movie).get_id();
        ObjectId moviePartyId = ObjectId.get();
        MongoWatchParty mongoWatchParty = new MongoWatchParty(userIds, ratingsMongoIds, movieId, dateObj, moviePartyId);

        watchPartyDao.insertWatchParty(mongoWatchParty);
    }

    public ExternalFacingMovieParty getWatchParty(ObjectId mongoId) {
        Document document = watchPartyDao.getWatchPartyById(mongoId);
        MongoWatchParty mongoWatchParty = CustomWatchPartyDeserializer.convertToWatchParty(document);

        return new ExternalFacingMovieParty(watchPartyBuilder.buildInternalWatchParty(mongoWatchParty));
    }

    public List<ExternalFacingMovieParty> getWatchParties(String username) {
        User user = userService.getUserByName(username);
        FindIterable<Document> documents = watchPartyDao.getWatchPartiesForUser(user.get_id());

        List<MongoWatchParty> mongoWatchParties = new ArrayList<>();
        for(Document document: documents) {
            mongoWatchParties.add(CustomWatchPartyDeserializer.convertToWatchParty(document));
        }

        return mongoWatchParties.stream().map(watchPartyBuilder::buildInternalWatchParty).map(ExternalFacingMovieParty::new).collect(Collectors.toList());
    }
}
