package dog.jake.watchparty.watchpartyutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import dog.jake.movie.moiveutils.Movie;
import dog.jake.ratings.RatingService;
import dog.jake.ratings.ratingutils.InternalRating;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InternalWatchParty {

    public static class InternalWatchPartyBuilder {
        private final RatingService ratingService;

        public InternalWatchPartyBuilder(RatingService ratingService) {
            this.ratingService = ratingService;
        }

        public InternalWatchParty buildInternalWatchParty(MongoWatchParty mongoWatchParty) {
            InternalWatchParty watchParty = new InternalWatchParty();
            watchParty.setDate(mongoWatchParty.getDate());

            List<ObjectId> ratings = mongoWatchParty.getRatings();
            List<InternalRating> internalRatings = new ArrayList<>();
            for(ObjectId rating: ratings) {
                internalRatings.add(ratingService.getRating(rating));
            }
            watchParty.setRatings(internalRatings);
            watchParty.setMovie(internalRatings.get(0).getMovie());
            return watchParty;
        }
    }
    private InternalWatchParty() {}

    @JsonProperty("ratings")
    private List<InternalRating> ratings;

    @JsonProperty("date")
    private Date date;

    @JsonProperty
    private Movie movie;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<InternalRating> getRatings() {
        return ratings;
    }

    public void setRatings(List<InternalRating> ratings) {
        this.ratings = ratings;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
