package dog.jake.watchparty.watchpartyutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dog.jake.serializationutils.CustomDateSerializer;
import dog.jake.serializationutils.ListObjectIdJsonSerializer;
import dog.jake.serializationutils.ObjectIdJsonSerializer;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

public class MongoWatchParty {

    @JsonProperty
    @JsonSerialize(using = ListObjectIdJsonSerializer.class)
    private List<ObjectId> users;
    @JsonProperty
    @JsonSerialize(using = ListObjectIdJsonSerializer.class)
    private List<ObjectId> ratings;
    @JsonProperty
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId movie;
    @JsonProperty
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date date;
    @JsonProperty("_id")
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId id;

    private MongoWatchParty() {
    }

    public MongoWatchParty(List<ObjectId> users, List<ObjectId> ratings, ObjectId movie, Date date, ObjectId moviePartyId) {
        this.users = users;
        this.ratings = ratings;
        this.movie = movie;
        this.date = date;
        this.id = moviePartyId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public List<ObjectId> getRatings() {
        return ratings;
    }

    public void setRatings(List<ObjectId> ratings) {
        this.ratings = ratings;
    }

    public ObjectId getMovie() {
        return movie;
    }

    public void setMovie(ObjectId movie) {
        this.movie = movie;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<ObjectId> getUsers() {
        return users;
    }

    public void setUsers(List<ObjectId> users) {
        this.users = users;
    }

    public static class MongoWatchPartyBuilder {
        private final ObjectId watchPartyId;
        private List<ObjectId> ratings;
        private ObjectId movieId;
        private Date date;
        private List<ObjectId> users;

        public MongoWatchPartyBuilder(ObjectId watchPartyId) {
            this.watchPartyId = watchPartyId;
        }

        public MongoWatchPartyBuilder withRatings(List<ObjectId> ratings) {
            this.ratings = ratings;
            return this;
        }

        public MongoWatchPartyBuilder withUsers(List<ObjectId> users) {
            this.users = users;
            return this;
        }

        public MongoWatchPartyBuilder withDate(Date date) {
            this.date = date;
            return this;
        }

        public MongoWatchPartyBuilder withMovieId(ObjectId movieId) {
            this.movieId = movieId;
            return this;
        }

        public MongoWatchParty build() {
            return new MongoWatchParty(users, ratings, movieId, date, watchPartyId);
        }
    }
}
