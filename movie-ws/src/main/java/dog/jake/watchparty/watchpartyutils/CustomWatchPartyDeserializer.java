package dog.jake.watchparty.watchpartyutils;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomWatchPartyDeserializer {
    private static final Logger LOG = LoggerFactory.getLogger(CustomWatchPartyDeserializer.class);

    public static MongoWatchParty convertToWatchParty(Document document) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        MongoWatchParty.MongoWatchPartyBuilder builder = new MongoWatchParty.MongoWatchPartyBuilder(new ObjectId(document.getString("_id")));

        List<String> ratings = document.getList("ratings", String.class);
        List<ObjectId> ratingObjs = new ArrayList<>();
        for (String rating : ratings) {
            ratingObjs.add(new ObjectId(rating));
        }

        List<String> users = document.getList("users", String.class);
        List<ObjectId> userObjs = new ArrayList<>();
        for (String user : users) {
            userObjs.add(new ObjectId(user));
        }

        builder.withUsers(userObjs);
        builder.withRatings(ratingObjs);
        builder.withMovieId(new ObjectId(document.getString("movie")));


        try {
            builder.withDate(dateFormat.parse(document.getString("date")));
        } catch (ParseException e) {
            LOG.error("Unable to parse date {}",document.getString("date"), e);
        }
        return builder.build();
    }
}
