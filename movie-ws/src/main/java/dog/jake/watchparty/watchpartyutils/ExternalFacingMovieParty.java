package dog.jake.watchparty.watchpartyutils;


import com.fasterxml.jackson.annotation.JsonProperty;
import dog.jake.ratings.ratingutils.ExternalFacingRating;
import dog.jake.ratings.ratingutils.InternalRating;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ExternalFacingMovieParty {

    @JsonProperty
    private String date;

    @JsonProperty
    private String movieName;

    @JsonProperty
    private List<ExternalFacingRating> ratings;


    public ExternalFacingMovieParty(InternalWatchParty internalWatchParty) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<ExternalFacingRating> ratings = new ArrayList<>();
        for(InternalRating internalRating: internalWatchParty.getRatings()) {
            ratings.add(new ExternalFacingRating(internalRating));
        }
        this.ratings = ratings;
        this.movieName = internalWatchParty.getMovie().getTitle();
        this.date = dateFormat.format(internalWatchParty.getDate());
    }
}
