package dog.jake.users;

import dog.jake.users.dao.UserDao;
import dog.jake.users.userutils.CustomUserDeserializer;
import dog.jake.users.userutils.User;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserService {
    private final UserDao userDao;
    private final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Inject
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public User getUserByName(String name) {
        Document userBSON = userDao.getUserByName(name);

        return CustomUserDeserializer.convertToUser(userBSON);
    }

    public User getUserById(ObjectId userId) {
        Document userBSON = userDao.getUserById(userId);

        return CustomUserDeserializer.convertToUser(userBSON);
    }

    public void insertNewUserToDatabase(String username, String password, String email) {
        LOG.debug("inserting new user into the database: {}", username);
        User.UserBuilder userBuilder = new User.UserBuilder(username);
        userBuilder.withObjectId(ObjectId.get());
        userBuilder.withEmail(email);
        userBuilder.withPassword(password);
        User user = userBuilder.build();
        userDao.insertNewUser(user);
    }
}
