package dog.jake.users.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dog.jake.users.userutils.User;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.inject.Inject;

import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class MongoUserDao implements UserDao{

    private MongoDatabase database;
    private MongoClient mongoClient;
    private MongoCollection<Document> collection;
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    public MongoUserDao() {
        mongoClient = MongoClients.create("mongodb+srv://movie-ws:movie-ws-password@moviecluster-xbgjb.mongodb.net/test?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("dog_database");
        collection = database.getCollection("users");
    }


    @Override
    public Document getUserByName(String username) {
        return collection.find(eq("username", username)).first();
    }

    @Override
    public Document getUserById(ObjectId id) {
        return collection.find(eq("_id", id.toString())).first();
    }

    @Override
    public void insertNewUser(User userToInsert) {
        Map<String, Object> map = mapper.convertValue(userToInsert, Map.class);
        Document userDoc = new Document(map);
        collection.insertOne(userDoc);
    }
}
