package dog.jake.users.dao;

import dog.jake.users.userutils.User;
import org.bson.Document;
import org.bson.types.ObjectId;

public interface UserDao {
    Document getUserByName(String name);
    Document getUserById(ObjectId id);
    void insertNewUser(User userToInsert);
}
