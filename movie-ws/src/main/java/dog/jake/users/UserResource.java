package dog.jake.users;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dog.jake.users.userutils.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserService userService;
    private final ObjectMapper mapper;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
        mapper = new ObjectMapper();
    }

    @Path("/insertNewUser")
    @POST
    @Operation(summary = "inserts a new user into the database")
    @Tag(name = "user")
    public Response insertNewUser(@QueryParam("username") String username, @QueryParam("email") String email, @QueryParam("password") String password) {
        userService.insertNewUserToDatabase(username, password, email);
        return Response.status(Response.Status.CREATED).build();
    }

    @Path("/getUserByName/{name}")
    @GET
    @Operation(summary = "receives a user from the mongo database by the username entered")
    @Tag(name = "user")
    public Response getUserByName(@PathParam("name") String username) {
        User user = userService.getUserByName(username);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(user)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }

    @Path("/getUserById/{id}")
    @GET
    @Operation(summary = "receives a movie from the mongo database by the internal mongo id")
    @Tag(name = "user")
    public Response getUserById(@PathParam("id") String id) {
        ObjectId objectId = new ObjectId(id);
        User user = userService.getUserById(objectId);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(user)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }



}
