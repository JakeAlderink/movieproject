package dog.jake.users;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import dog.jake.users.dao.MongoUserDao;
import dog.jake.users.dao.UserDao;

import javax.inject.Named;

@Module
public abstract class UserModule {
    @Binds
    abstract UserDao bindMongoUserDao(MongoUserDao dao);

    @Binds @IntoSet @Named("instances") abstract Object resource(UserResource userResource);
}
