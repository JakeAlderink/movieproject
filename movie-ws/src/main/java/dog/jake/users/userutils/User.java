package dog.jake.users.userutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongodb.lang.Nullable;
import dog.jake.serializationutils.ObjectIdJsonSerializer;
import org.bson.types.ObjectId;

public class User {

    public static class UserBuilder{
        private String username;
        private ObjectId mongoid;
        private String password;
        private String email;

        public UserBuilder(String username) {
            this.username = username;
        }

        public UserBuilder withObjectId(ObjectId mongoid) {
            this.mongoid = mongoid;

            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;

            return this;
        }

        public UserBuilder withEmail(String email) {
            this.email = email;

            return this;
        }

        public User build(){
            User user = new User();
            user.set_id(mongoid);
            user.setUsername(username);
            user.setEmail(email);
            user.setPassword(password);
            return user;
        }
    }

    public User(){
    }


    @JsonProperty("_id")
    @JsonSerialize(using= ObjectIdJsonSerializer.class)
    @Nullable
    private ObjectId _id;

    @JsonProperty("username")
    private String username;

    @JsonProperty
    private String password;

    @JsonProperty
    private String email;

    @Nullable
    public ObjectId get_id() {
        return _id;
    }

    public void set_id(@Nullable ObjectId _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
