package dog.jake.users.userutils;

import org.bson.Document;
import org.bson.types.ObjectId;


public class CustomUserDeserializer {
    private CustomUserDeserializer() {
    }

    public static User convertToUser(Document userBSON) {
        User.UserBuilder userBuilder = new User.UserBuilder(userBSON.getString("username"));
        userBuilder.withObjectId(new ObjectId(userBSON.getString("_id")));
        userBuilder.withPassword(userBSON.getString("password"));
        userBuilder.withEmail(userBSON.getString("email"));
        return userBuilder.build();
    }
}
