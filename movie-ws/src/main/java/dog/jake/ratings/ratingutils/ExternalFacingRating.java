package dog.jake.ratings.ratingutils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExternalFacingRating {
    @JsonProperty
    private String username;
    @JsonProperty
    private Double rating;

    public ExternalFacingRating(InternalRating internalRating) {
        this.rating = internalRating.getRating();
        this.username = internalRating.getUser().getUsername();
    }
}
