package dog.jake.ratings.ratingutils;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CustomRatingDeserializer {
    private static final Logger LOG = LoggerFactory.getLogger(CustomRatingDeserializer.class);
    private CustomRatingDeserializer() {
    }

    public static MongoRating convertToRating(Document document) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        MongoRating.MongoRatingBuilder mongoRatingBuilder = new MongoRating.MongoRatingBuilder(document.getDouble("rating"));
        mongoRatingBuilder.withId(new ObjectId(document.getString("_id")));
        mongoRatingBuilder.withMovieId(new ObjectId(document.getString("movieId")));
        mongoRatingBuilder.withUserId(new ObjectId(document.getString("userId")));
        try {
            mongoRatingBuilder.withDate(dateFormat.parse(document.getString("date")));
        } catch (ParseException e) {
            LOG.error("unable to parse date {}", document.getString("date"), e);
        }
        return mongoRatingBuilder.build();
    }

}
