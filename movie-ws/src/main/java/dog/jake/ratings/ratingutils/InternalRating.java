package dog.jake.ratings.ratingutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import dog.jake.movie.MovieService;
import dog.jake.movie.moiveutils.Movie;
import dog.jake.users.UserService;
import dog.jake.users.userutils.User;

import java.util.Date;


public class InternalRating {


    public static class InternalRatingBuilder {
        private final MovieService movieService;
        private final UserService userService;

        public InternalRatingBuilder(MovieService movieService, UserService userService) {
            this.movieService = movieService;
            this.userService = userService;
        }

        public InternalRating buildInternalRating(MongoRating mongoRating) {
            InternalRating rating = new InternalRating();
            rating.setMovie(movieService.getMovie(mongoRating.getMovieId()));
            rating.setUser(userService.getUserById(mongoRating.getUserId()));
            rating.setRating(mongoRating.getRating());
            rating.setDate(mongoRating.getDate());
            return rating;
        }

    }

    @JsonProperty
    private Date date;

    @JsonProperty
    private Double rating;

    @JsonProperty("movie")
    private Movie movie;

    @JsonProperty("user")
    private User user;

    private InternalRating(){
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
