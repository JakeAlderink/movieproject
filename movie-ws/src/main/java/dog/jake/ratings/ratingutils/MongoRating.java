package dog.jake.ratings.ratingutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dog.jake.serializationutils.CustomDateSerializer;
import dog.jake.serializationutils.ObjectIdJsonSerializer;
import org.bson.types.ObjectId;

import java.util.Date;

public class MongoRating {

    public static class MongoRatingBuilder {
        private Double rating;
        private ObjectId userId;
        private ObjectId movieId;
        private ObjectId id;
        private Date date;

        public MongoRatingBuilder(Double rating) {
            this.rating = rating;
        }

        public MongoRatingBuilder withUserId(ObjectId userId) {
            this.userId = userId;

            return this;
        }

        public MongoRatingBuilder withMovieId(ObjectId movieId) {
            this.movieId = movieId;

            return this;
        }

        public MongoRatingBuilder withId(ObjectId id) {
            this.id = id;

            return this;
        }

        public MongoRatingBuilder withDate(Date date) {
            this.date = date;

            return this;
        }

        public MongoRating build() {
            MongoRating mongoRatingInstance = new MongoRating(rating);
            mongoRatingInstance.setId(id);
            mongoRatingInstance.setMovieId(movieId);
            mongoRatingInstance.setUserId(userId);
            mongoRatingInstance.setDate(date);
            return mongoRatingInstance;
        }
    }

    @JsonProperty
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date date;

    @JsonProperty
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId userId;

    @JsonProperty("_id")
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId id;

    @JsonProperty
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId movieId;

    @JsonProperty
    private Double rating;

    public MongoRating(Double rating) {
        this.rating = rating;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getMovieId() {
        return movieId;
    }

    public void setMovieId(ObjectId movieId) {
        this.movieId = movieId;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
