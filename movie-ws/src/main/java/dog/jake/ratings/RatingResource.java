package dog.jake.ratings;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dog.jake.ratings.ratingutils.InternalRating;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Path("/ratings")
@Produces(MediaType.APPLICATION_JSON)
public class RatingResource {
    private final RatingService ratingService;
    private final ObjectMapper mapper;

    @Inject
    public RatingResource(RatingService ratingService) {
        this.ratingService = ratingService;
        mapper = new ObjectMapper();
    }

    @Path("/getRatings/{id}")
    @GET
    @Operation(summary = "receives a rating from the mongo database by the internal mongoId")
    @Tag(name = "rating")
    public Response getRatings(@PathParam("id") String id) {
        ObjectId objectId = new ObjectId(id);
        InternalRating rating = ratingService.getRating(objectId);
        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(rating)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }

    @Path("/getUserRatings/{name}")
    @GET
    @Operation(summary = "receives all ratings of a given user")
    @Tag(name = "rating")
    public Response getRatingsByUser(@PathParam("name") String username) {
        List<InternalRating> internalRatings = ratingService.getRatings(username);
        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(internalRatings)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }

    @Path("/insertRating")
    @POST
    @Operation(summary = "inserts a rating of a movie for a user ")
    @Tag(name = "rating")
    public Response insertRating(@QueryParam("movieName") String movieName,
                                 @QueryParam("username") String username,
                                 @QueryParam("ratingValue") Double ratingValue,
                                 @Parameter(description = "needs to be in format \"YYYY-MM-DD\"") @QueryParam("date") String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateObj = null;
        try {
            dateObj =  dateFormat.parse(date);
        } catch (ParseException e) {
            Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        ratingService.insertRating(ratingValue, movieName, username, dateObj);
        return Response.status(Response.Status.CREATED).build();
    }


}
