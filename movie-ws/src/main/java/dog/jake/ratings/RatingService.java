package dog.jake.ratings;


import com.mongodb.client.FindIterable;
import dog.jake.movie.moiveutils.Movie;
import dog.jake.movie.MovieService;
import dog.jake.ratings.dao.RatingDao;
import dog.jake.ratings.ratingutils.CustomRatingDeserializer;
import dog.jake.ratings.ratingutils.InternalRating;
import dog.jake.ratings.ratingutils.MongoRating;
import dog.jake.users.UserService;
import dog.jake.users.userutils.User;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Singleton
public class RatingService {

    private final RatingDao ratingDao;
    private final MovieService movieService;
    private final UserService userService;
    private final InternalRating.InternalRatingBuilder ratingBuilder;
    private final Logger LOG = LoggerFactory.getLogger(RatingService.class);

    @Inject
    public RatingService(RatingDao ratingDao, UserService userService, MovieService movieService) {
        this.ratingDao = ratingDao;
        this.movieService = movieService;
        this.userService = userService;
        ratingBuilder = new InternalRating.InternalRatingBuilder(movieService, userService);
    }

    public InternalRating getRating(ObjectId objectId) {
        Document document = ratingDao.getRating(objectId);

        MongoRating mongoRating = CustomRatingDeserializer.convertToRating(document);
        return ratingBuilder.buildInternalRating(mongoRating);
    }

    public List<InternalRating> getRatings(String username) {
        User user = userService.getUserByName(username);
        FindIterable<Document> documents = ratingDao.getRatings(user.get_id());

        List<InternalRating> internalRatings = new ArrayList<>();
        for(Document document : documents) {
            internalRatings.add(ratingBuilder.buildInternalRating(CustomRatingDeserializer.convertToRating(document)));
        }
        return internalRatings;
    }

    public ObjectId insertRating(Double ratingVal, String movieName, String username, Date date) {
        LOG.debug("inserting new rating into database for user {}", username);
        Movie movie = movieService.getMovie(movieName);
        User user = userService.getUserByName(username);


        MongoRating mongoRating = new MongoRating(ratingVal);
        mongoRating.setId(ObjectId.get());
        mongoRating.setMovieId(movie.get_id());
        mongoRating.setUserId(user.get_id());
        mongoRating.setDate(date);
        ratingDao.insertRating(mongoRating);
        return mongoRating.getId();
    }

}
