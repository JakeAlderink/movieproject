package dog.jake.ratings.dao;

import com.mongodb.client.FindIterable;
import dog.jake.ratings.ratingutils.MongoRating;
import org.bson.Document;
import org.bson.types.ObjectId;

public interface RatingDao {
    Document getRating(ObjectId id);

    void insertRating(MongoRating mongoRating);

    FindIterable<Document> getRatings(ObjectId id);
}
