package dog.jake.ratings.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dog.jake.ratings.ratingutils.MongoRating;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.inject.Inject;

import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class MongoRatingDao implements RatingDao {

    private final MongoDatabase database;
    private final MongoClient mongoClient;
    private final MongoCollection<Document> collection;
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    public MongoRatingDao() {
        mongoClient = MongoClients.create("mongodb+srv://movie-ws:movie-ws-password@moviecluster-xbgjb.mongodb.net/test?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("dog_database");
        collection = database.getCollection("ratings");
    }


    @Override
    public Document getRating(ObjectId id) {
        return collection.find(eq("_id", id.toString())).first();
    }

    @Override
    public void insertRating(MongoRating mongoRating) {
        Map<String, Object> ratingMap = mapper.convertValue(mongoRating, Map.class);
        Document document = new Document(ratingMap);
        collection.insertOne(document);
    }

    @Override
    public FindIterable<Document> getRatings(ObjectId userId) {
        return collection.find(eq("userId", userId.toString()));
    }
}
