package dog.jake.ratings;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import dog.jake.ratings.dao.MongoRatingDao;
import dog.jake.ratings.dao.RatingDao;

import javax.inject.Named;

@Module
public abstract class RatingModule {
    @Binds abstract RatingDao bindMongoRatingDao(MongoRatingDao mongoRatingDao);

    @Binds @IntoSet @Named("instances") abstract Object resource(RatingResource ratingResource);
}
