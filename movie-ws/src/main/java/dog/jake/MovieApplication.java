package dog.jake;

import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("rest")
@OpenAPIDefinition( servers = {@Server(url = "/movie-ws")})

public class MovieApplication extends ResourceConfig {
    public MovieApplication() {
        MovieComponent component = DaggerMovieComponent.create();
        register(OpenApiResource.class, AcceptHeaderOpenApiResource.class);
        registerInstances(component.instances());
    }
}
