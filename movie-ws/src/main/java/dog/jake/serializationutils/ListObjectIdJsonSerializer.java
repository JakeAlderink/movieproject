package dog.jake.serializationutils;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.bson.types.ObjectId;

public class ListObjectIdJsonSerializer extends JsonSerializer<List<ObjectId>> {
    @Override
    public void serialize(List<ObjectId> list, JsonGenerator j, SerializerProvider s) throws IOException, JsonProcessingException {
        if(list == null) {
            j.writeNull();
        } else {
            j.writeStartArray();
            for(ObjectId o : list)
            {
                j.writeString(o.toString());
            }
            j.writeEndArray();
        }
    }
}
