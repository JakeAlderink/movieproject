package dog.jake.movie.moiveutils;

import dog.jake.omdbapi.utils.MovieApiResponse;
import dog.jake.omdbapi.utils.RatingsItem;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Arrays;
import java.util.List;

public class CustomMovieDeserializer {
    private CustomMovieDeserializer() {}
    public static Movie convertToMovie(Document mongoReturnObject) {
        Movie.MovieBuilder movieBuilder = new Movie.MovieBuilder(mongoReturnObject.getString("title"));
        movieBuilder.withObjectId(new ObjectId(mongoReturnObject.getString("_id")));
        movieBuilder.withActors(mongoReturnObject.getList("actors",String.class));
        movieBuilder.withDirectors(mongoReturnObject.getList("directors",String.class));
        movieBuilder.withImdbScore(mongoReturnObject.getDouble("iMDBScore"));
        movieBuilder.withRottenTomatoesScore(mongoReturnObject.getDouble("rottenTomatoesScore"));
        movieBuilder.withSortTitle(mongoReturnObject.getString("sortTitle"));
        movieBuilder.withRuntime(mongoReturnObject.getDouble("runTime"));
        movieBuilder.withYear(mongoReturnObject.getInteger("year"));

        return movieBuilder.build();
    }

    public static Movie convertToMovie(MovieApiResponse apiObj) {
        Movie.MovieBuilder movieBuilder = new Movie.MovieBuilder(apiObj.getTitle());
        movieBuilder.withObjectId(ObjectId.get());
        movieBuilder.withActors(convertToList(apiObj.getActors()));
        movieBuilder.withDirectors(convertToList(apiObj.getDirector()));
        movieBuilder.withImdbScore(Double.parseDouble(apiObj.getImdbRating()));
        movieBuilder.withRottenTomatoesScore(getRottenTomatoes(apiObj));
        movieBuilder.withSortTitle(apiObj.getTitle());
        movieBuilder.withRuntime(getRunTime(apiObj.getRuntime()));
        movieBuilder.withYear(Integer.parseInt(apiObj.getYear()));

        return movieBuilder.build();
    }

    private static List<String> convertToList(String ogString) {
        return Arrays.asList(ogString.split(","));
    }

    private static Double getRottenTomatoes(MovieApiResponse apiObj) {
        for(RatingsItem source :apiObj.getRatings()) {
            if(source.getSource().equals("Rotten Tomatoes")) {
                return Double.parseDouble(source.getValue().replace("%",""));
            }
        }
        return -0.1;
    }

    private static Double getRunTime(String runtime) {
        return Double.parseDouble(runtime.replaceAll("[^\\d.]", ""));
    }

}
