package dog.jake.movie.moiveutils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import dog.jake.serializationutils.ObjectIdJsonSerializer;
import org.bson.types.ObjectId;

import javax.annotation.Nullable;
import java.util.List;

public class Movie {

    public static class MovieBuilder {
        private ObjectId mongoId;
        private String title;
        private List<String> actors;
        private List<String> directors;
        private Double runTime;
        private String sortTitle;
        private Double iMDbScore;
        private Double rottenTomatoesScore;
        private Integer year;


        public MovieBuilder(String movieName) {
            this.title = movieName;
        }

        public MovieBuilder withObjectId(ObjectId objectId) {
            this.mongoId = objectId;

            return this;
        }

        public MovieBuilder withActors(List<String> actors) {
            this.actors = actors;

            return this;
        }

        public MovieBuilder withDirectors(List<String> directors) {
            this.directors = directors;

            return this;
        }

        public MovieBuilder withRuntime(Double runTime) {
            this.runTime = runTime;

            return this;
        }

        public MovieBuilder withSortTitle(String sortTitle) {
            this.sortTitle = sortTitle;

            return this;
        }

        public MovieBuilder withImdbScore(Double iMDbScore) {
            this.iMDbScore = iMDbScore;

            return this;
        }

        public MovieBuilder withRottenTomatoesScore(Double rottenTomatoesScore) {
            this.rottenTomatoesScore = rottenTomatoesScore;

            return this;
        }

        public MovieBuilder withYear(Integer year) {
            this.year = year;

            return this;
        }

        public Movie build() {
            Movie movie = new Movie();
            movie.setActors(actors);
            movie.setDirectors(directors);
            movie.setTitle(title);
            movie.setiMDbScore(iMDbScore);
            movie.setRunTime(runTime);
            movie.setRottenTomatoesScore(rottenTomatoesScore);
            movie.setSortTitle(sortTitle);
            movie.set_id(mongoId);
            movie.setYear(year);
            return movie;
        }
    }


    @JsonProperty("_id")
    @JsonSerialize(using= ObjectIdJsonSerializer.class)
    @Nullable
    private ObjectId _id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("actors")
    @Nullable
    private List<String> actors;

    @JsonProperty("directors")
    @Nullable
    private List<String> directors;

    @JsonProperty("runTime")
    @Nullable
    private Double runTime;

    @JsonProperty("sortTitle")
    @Nullable
    private String sortTitle;

    @JsonProperty("iMDBScore")
    @Nullable
    private Double iMDbScore;

    @JsonProperty("rottenTomatoesScore")
    @Nullable
    private Double rottenTomatoesScore;

    @JsonProperty("year")
    @Nullable
    private Integer year;

    public Movie(){
    }

    @Nullable
    public Integer getYear() {
        return year;
    }

    public void setYear(@Nullable Integer year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Nullable
    public List<String> getActors() {
        return actors;
    }

    public void setActors(@Nullable List<String> actors) {
        this.actors = actors;
    }

    @Nullable
    public List<String> getDirectors() {
        return directors;
    }

    public void setDirectors(@Nullable List<String> directors) {
        this.directors = directors;
    }

    @Nullable
    public Double getRunTime() {
        return runTime;
    }

    public void setRunTime(@Nullable Double runTime) {
        this.runTime = runTime;
    }

    @Nullable
    public String getSortTitle() {
        return sortTitle;
    }

    public void setSortTitle(@Nullable String sortTitle) {
        this.sortTitle = sortTitle;
    }

    @Nullable
    public Double getiMDbScore() {
        return iMDbScore;
    }

    public void setiMDbScore(@Nullable Double iMDbScore) {
        this.iMDbScore = iMDbScore;
    }

    @Nullable
    public Double getRottenTomatoesScore() {
        return rottenTomatoesScore;
    }

    public void setRottenTomatoesScore(@Nullable Double rottenTomatoesScore) {
        this.rottenTomatoesScore = rottenTomatoesScore;
    }

    @Nullable
    public ObjectId get_id() {
        return _id;
    }

    public void set_id(@Nullable ObjectId _id) {
        this._id = _id;
    }

}
