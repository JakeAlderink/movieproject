package dog.jake.movie;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dog.jake.movie.moiveutils.Movie;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/movie")
@Produces(MediaType.APPLICATION_JSON)
public class MovieResource {

    private final MovieService movieService;
    private final ObjectMapper mapper;

    @Inject
    public MovieResource(MovieService movieService) {
        this.movieService = movieService;
        mapper = new ObjectMapper();
    }

    @Path("/insertMovieAutoFill/{name}")
    @POST
    @Operation(summary = "insert a new movie to database, have values autofilled (buggy v0)")
    @Tag(name = "movie")
    public Response insertMovieToDbAutoFill(@Nonnull @PathParam("name") String movieName) {
        movieService.insertMovieWithJustTheName(movieName);
        return Response.status(Response.Status.CREATED).build();
    }

    @Path("/insertMoviesAutoFill")
    @POST
    @Operation(summary = "insert movies to database, have values autofilled (buggy v0)")
    @Tag(name = "movie")
    public Response insertMovieToDbAutoFill(@Nonnull @QueryParam("names") List<String> movieNames) {
        for(String name: movieNames) {
            movieService.insertMovieWithJustTheName(name);
        }
        return Response.status(Response.Status.CREATED).build();
    }

    @Path("/insertMovie")
    @POST
    @Operation(summary = "insert a new movie into the database")
    @Tag(name = "movie")
    public Response insertMovieToDb(@Nonnull @QueryParam("movieName") String movieName,
                                    @QueryParam("actors") List<String> actors,
                                    @QueryParam("directors") List<String> directors,
                                    @QueryParam("runTime") Double runTime,
                                    @QueryParam("year") Integer year,
                                    @QueryParam("imdbScore") Double imdbScore,
                                    @QueryParam("rottenTomatoesScore") Double rottenTomatoesScore) {
        movieService.insertMovieIntoDatabase(movieName,actors,directors,runTime,year,imdbScore,rottenTomatoesScore);
        return Response.status(Response.Status.CREATED).build();
    }

    @Path("/getMovieById/{id}")
    @GET
    @Operation(summary = "receives a movie from the mongo database by the internal mongoId")
    @Tag(name = "movie")
    public Response getMovieByObjectId(@PathParam("id") String movieId) {
        ObjectId objectId = new ObjectId(movieId);
        Movie movie = movieService.getMovie(objectId);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(movie)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }


    @Path("/getMovieByName/{name}")
    @GET
    @Operation(summary = "receives a movie from the mongo database by the full title name entered")
    @Tag(name = "movie")
    public Response getMovie(@PathParam("name") String movieName) {
        Movie movie = movieService.getMovie(movieName);

        try {
            return Response.status(Response.Status.OK).entity(mapper.writeValueAsString(movie)).build();
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
    }

    @Path("/getFirstMovie")
    @GET
    @Operation(summary = "returns first movie mongo returns" )
    @Tag(name = "movie")
    public Response getFirstMovie() {
        Document document = movieService.getDocument();
        return Response.status(Response.Status.OK).entity(document.toJson()).build();
    }

}
