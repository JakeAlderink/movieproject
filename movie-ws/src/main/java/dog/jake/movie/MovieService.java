package dog.jake.movie;

import dog.jake.movie.moiveutils.Movie;
import dog.jake.movie.dao.MovieDao;
import dog.jake.movie.moiveutils.CustomMovieDeserializer;
import dog.jake.omdbapi.OmdbService;
import dog.jake.omdbapi.utils.MovieApiResponse;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class MovieService {
    private final MovieDao mongoMovieDao;
    private final OmdbService omdbService;
    private final Logger LOG = LoggerFactory.getLogger(MovieService.class);

    @Inject
    public MovieService(MovieDao mongoMovieDao, OmdbService omdbService) {
        this.mongoMovieDao = mongoMovieDao;
        this.omdbService = omdbService;
    }

    public Document getDocument() {
        return mongoMovieDao.returnFirstDocument();
    }

    public Movie getMovie(ObjectId mongoId) {
        Document movieBSON =  mongoMovieDao.getMovie(mongoId);

        return CustomMovieDeserializer.convertToMovie(movieBSON);
    }

    public Movie getMovie(String movieName) {
        Document movieBSON =  mongoMovieDao.getMovie(movieName);

        return  CustomMovieDeserializer.convertToMovie(movieBSON);
    }

    public void insertMovieWithJustTheName(String movieName) {
        LOG.debug("Entering new movie into the database named {}", movieName);
        MovieApiResponse apiObj = omdbService.getMovieInfoFromName(movieName);
        mongoMovieDao.insertMovie(CustomMovieDeserializer.convertToMovie(apiObj));
    }

    public void insertMovieIntoDatabase(String movieName, List<String> actors, List<String> directors, Double runTime, Integer year, Double imdbScore, Double rottenTomatoesScore) {
        LOG.debug("Entering a new movie into the database manually {}", movieName);
        Movie.MovieBuilder movieBuilder = new Movie.MovieBuilder(movieName);
        movieBuilder.withYear(year);
        movieBuilder.withRuntime(runTime);
        movieBuilder.withSortTitle(movieName);
        movieBuilder.withRottenTomatoesScore(rottenTomatoesScore);
        movieBuilder.withImdbScore(imdbScore);
        movieBuilder.withActors(actors);
        movieBuilder.withDirectors(directors);
        movieBuilder.withObjectId(ObjectId.get());
        Movie movieToInsert = movieBuilder.build();
        mongoMovieDao.insertMovie(movieToInsert);
    }
}
