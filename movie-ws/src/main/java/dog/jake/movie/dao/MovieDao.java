package dog.jake.movie.dao;


import dog.jake.movie.moiveutils.Movie;
import org.bson.Document;
import org.bson.types.ObjectId;

public interface MovieDao {
    Document returnFirstDocument();
    Document getMovie(String movieName);
    Document getMovie(ObjectId movieId);
    void insertMovie(Movie movieToInsert);
}
