package dog.jake.movie.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dog.jake.movie.moiveutils.Movie;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.inject.Inject;

import java.util.Map;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;


public class MongoMovieDao implements MovieDao {

    private final MongoDatabase database;
    private final MongoClient mongoClient;
    private final MongoCollection<Document> collection;
    private final ObjectMapper mapper = new ObjectMapper();

    @Inject
    public MongoMovieDao() {
        mongoClient = MongoClients.create("mongodb+srv://movie-ws:movie-ws-password@moviecluster-xbgjb.mongodb.net/test?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("dog_database");
        collection = database.getCollection("movies");
    }

    @Override
    public Document returnFirstDocument() {
        return collection.find().first();
    }

    @Override
    public Document getMovie(String movieName) {
        return collection.find(eq("title", movieName)).first();
    }

    @Override
    public Document getMovie(ObjectId movieId) {
        return collection.find(eq("_id", movieId.toString())).first();
    }

    @Override
    public void insertMovie(Movie movieToInsert) {
        Document document = collection.find(and(eq("title", movieToInsert.getTitle()),eq("year",movieToInsert.getYear()))).first();

        if( document != null) {
            collection.deleteOne(and(eq("title", movieToInsert.getTitle()), eq("year", movieToInsert.getYear())));
            movieToInsert.set_id(new ObjectId(document.getString("_id")));
        }

        Map<String, Object> map = mapper.convertValue(movieToInsert, Map.class);
        Document movieDoc = new Document(map);
        collection.insertOne(movieDoc);
    }
}
