package dog.jake.movie;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;
import dog.jake.movie.dao.MongoMovieDao;
import dog.jake.movie.dao.MovieDao;

import javax.inject.Named;

@Module
public abstract class MovieModule {
    @Binds abstract MovieDao bindMongoSalesDao(MongoMovieDao dao);

    @Binds @IntoSet @Named("instances") abstract Object resource(MovieResource movieResource);
}
