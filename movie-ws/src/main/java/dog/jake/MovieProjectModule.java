package dog.jake;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;

import javax.inject.Named;

@Module
public abstract class MovieProjectModule {
    @Binds @IntoSet @Named("instances") abstract Object bindLifeCyclePlugin(LifecyclePlugin plugin);
}
