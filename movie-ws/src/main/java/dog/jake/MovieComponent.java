package dog.jake;

import dagger.Component;
import dog.jake.omdbapi.OmdbModule;
import dog.jake.ratings.RatingModule;
import dog.jake.movie.MovieModule;
import dog.jake.users.UserModule;
import dog.jake.watchparty.WatchPartyModule;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Set;

@Component(modules = {MovieProjectModule.class, MovieModule.class, UserModule.class, RatingModule.class, WatchPartyModule.class, OmdbModule.class})

@Singleton
interface MovieComponent {
    @Named("instances")
    Set<Object> instances();
}
