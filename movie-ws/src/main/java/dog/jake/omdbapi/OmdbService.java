package dog.jake.omdbapi;

import com.google.common.annotations.VisibleForTesting;
import com.google.gson.Gson;
import dog.jake.omdbapi.dao.OmdbDao;
import dog.jake.omdbapi.utils.MovieApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class OmdbService {
    private static final String secondaryKey = "5b4d1f75";
    private static final String url = "http://www.omdbapi.com/?apikey=be6c0ce9&";
    private final OmdbDao omdbDao;

    private final Logger LOG = LoggerFactory.getLogger(OmdbService.class);
    @Inject
    public OmdbService(OmdbDao omdbDao){
        this.omdbDao = omdbDao;
    }

    @VisibleForTesting
    public MovieApiResponse getMovieInfoFromName(String name) {
        String urlName = name.replaceAll(" ", "+").toLowerCase();
        String finalUrl = url + "t="+urlName;
        LOG.debug("attempting connection to Omdb with url {}", finalUrl);

        StringBuffer content = omdbDao.getStringBuffer(finalUrl);

        if(content != null) {
            Gson gson = new Gson();
            String finalString = content.toString();
            return gson.fromJson(finalString, MovieApiResponse.class);
        }
        return null;
    }




}
