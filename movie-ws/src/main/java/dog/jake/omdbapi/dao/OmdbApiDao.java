package dog.jake.omdbapi.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class OmdbApiDao implements OmdbDao{
    private final Logger LOG = LoggerFactory.getLogger(OmdbApiDao.class);

    @Inject
    public OmdbApiDao() {}

    @Override
    public StringBuffer getStringBuffer(String finalUrl) {
        URL url;
        HttpURLConnection connection;
        StringBuffer content;
        try {
            url = new URL(finalUrl);
            connection= (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            connection.disconnect();
        } catch (MalformedURLException e) {
            LOG.error("Received malformed url exception", e);
            return null;
        } catch (IOException e) {
            LOG.error("Error connecting to url", e);
            return null;
        }
        return content;
    }

}
