package dog.jake.omdbapi.dao;

public interface OmdbDao {
    StringBuffer getStringBuffer(String finalUrl);
}
