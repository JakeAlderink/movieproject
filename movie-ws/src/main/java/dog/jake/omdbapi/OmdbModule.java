package dog.jake.omdbapi;


import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dog.jake.omdbapi.dao.OmdbApiDao;
import dog.jake.omdbapi.dao.OmdbDao;

import javax.inject.Inject;
import javax.inject.Singleton;

@Module
public abstract class OmdbModule {

    @Binds abstract OmdbDao bindMongoSalesDao(OmdbApiDao dao);

    @Module
    public static class ModuleClass {
        @Provides
        @Singleton
        @Inject
        public OmdbService provideOmdbService(OmdbDao dao) {
            return new OmdbService(dao);
        }
    }
}
