package dog.jake.omdbapi;

import dog.jake.omdbapi.dao.OmdbApiDao;
import dog.jake.omdbapi.utils.MovieApiResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OmdbServiceTest {
    private final OmdbApiDao mockDao = mock(OmdbApiDao.class);
    private final String url = "http://www.omdbapi.com/?apikey=be6c0ce9&";
    private final OmdbService omdbService = new OmdbService(mockDao);
    @AfterEach
    public void reset() {
        Mockito.reset(mockDao);
    }

    @Test
    public void getMovieInfoFromName() {
        String movieName = "Johnny Test";
        String movieNameUrl = "t=johnny+test";
        StringBuffer stringBuffer = new StringBuffer("{\"Year\" : 2020}");
        when(mockDao.getStringBuffer(url+movieNameUrl)).thenReturn(stringBuffer);

        MovieApiResponse response = omdbService.getMovieInfoFromName(movieName);
        assertEquals("2020", response.getYear());
        assertNotNull(response);
        verify(mockDao, times(1)).getStringBuffer(url+movieNameUrl);
    }
}